describe('Conduits home page', function () {

    // it('assert that home page title is correct', function () {
    //     cy.visit('http://localhost:4200/')
    //     cy.title().should('include', 'Conduit')
 
    // })

    context('home page', function(){

        beforeEach(function(){
            cy.visit('http://localhost:4200/')
        })

        it('assert that navigation link to your feed is blocked', function(){
            cy.get('.feed-toggle > .nav > :nth-child(1) > .nav-link')
                .should('be.visible')
                    .and('contain', 'Your Feed')
                     .click()
            cy.url()
                .should('eq', 'http://localhost:4200/login')

        })


    
        
    })

 

})